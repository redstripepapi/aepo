var Testimonials = (function($){
  var el = {
    $testimonials: $('.testimonials-carousel')
  };

  function init(){
    el.$testimonials.owlCarousel({
      items: 1,
      loop: true,
      dots: true,
      nav: false,
    });
  }

  return {
    init: init
  };
})(jQuery);
