var Nav = (function($){
  var el = {
    $navIcon: $('.nav-icon'),
    $navPrimary: $('.nav-primary')
  };

  function init(){
    _bindUIactions();
  }

  function _bindUIactions(){
    el.$navIcon.on('click', function(e){
      el.$navPrimary.toggleClass('active');
      e.preventDefault();
    });
  }

  return {
    init: init
  };
})(jQuery);
