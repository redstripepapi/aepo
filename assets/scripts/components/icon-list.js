var IconList = (function($){
  var el = {
    $iconList: $('.icon-list')
  };

  var carousel = false;
  var rows = false;

  function init(){
    _initIconList();
    _bindUIactions();
  }

  function _bindUIactions(){
    $(window).on('resize', _initIconList);
  }

  function _initIconList(){
    if( $(window).width() >= 768 ) {
      if(carousel) {
        _destroyCarousel();
      }

      if(!rows){
        _initRows();
      }
    } else {
      if(rows){
        _destroyRows();
      }

      if(!carousel){
        _initCarousel();
      }
    }
  }

  function _initCarousel(){
    el.$iconList.owlCarousel({
      items: 1,
      loop: true,
      dots: true,
      center: true
    });

    el.$iconList.addClass('owl-carousel');
    el.$iconList.closest('.content-block').addClass('content-block-carousel');

    carousel = true;
  }

  function _destroyCarousel(){
    el.$iconList.trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
    el.$iconList.find('.owl-stage-outer').children().unwrap();
    el.$iconList.closest('.content-block').removeClass('content-block-carousel');
    carousel = false;
  }

  function _initRows(){
    var items = el.$iconList.children();
    for(var i = 0; i < items.length; i+=2) {
      items.slice(i, i+2).wrapAll("<div class='icon-list-row'></div>");
    }
    rows = true;
  }

  function _destroyRows(){
    el.$iconList.find('.icon-list-row').children().unwrap();
    rows = false;
  }

  return {
    init: init
  };
})(jQuery);
