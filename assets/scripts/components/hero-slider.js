var HeroSlider = (function($){
  var el = {
    $heroSlider: $('.hero-slider')
  };

  function init(){
    el.$heroSlider.owlCarousel({
      items: 1,
      loop: true,
      dots: true,
      nav: true,
      center: false,
      navText: ['<i class="aepo-icon aepo-icon--prev"></i>','<i class="aepo-icon aepo-icon--next"></i>']
    });
  }

  return {
    init: init
  };
})(jQuery);
