var Clients = (function($){
  var el = {
    $clients: $('.clients-carousel')
  };

  var carousel = false;

  function init(){
    _initClients();
    _bindUIactions();
  }

  function _bindUIactions(){
    $(window).on('resize', _initClients);
  }

  function _initClients(){
    if( $(window).width() >= 992 ){
      if( carousel ) {
        _destroyCarousel();
      }
    } else {
      if ( ! carousel ){
        _initCarousel();
      }
    }
  }

  function _initCarousel(){
    el.$clients.owlCarousel({
      loop: true,
      dots: true,
      center: true,
      responsive : {
        0 : {
          items: 2,
        },
        768 : {
          items: 3
        }
      }
    });
    el.$clients.addClass('owl-carousel');
    el.$clients.closest('.content-block').addClass('content-block-carousel');

    carousel = true;
  }

  function _destroyCarousel(){
    el.$clients.trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
    el.$clients.find('.owl-stage-outer').children().unwrap();
    el.$clients.removeClass('owl-carousel');
    el.$clients.closest('.content-block').removeClass('content-block-carousel');

    carousel = false;
  }

  return {
    init: init
  };
})(jQuery);
