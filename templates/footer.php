<footer class="content-info">
  <div class="container">
    <a class="brand" href="<?php echo bloginfo( 'url' ); ?>">
      <i class="aepo-icon aepo-icon--logomark"></i>
    </a>
    <span class="legal">All rights reserved. &copy;<?php echo date( 'Y' ); ?> <?php echo bloginfo( 'name' ); ?></span>
    <nav class="secondary-nav">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'nav']);
      endif;
      ?>
    </nav>
  </div>
</footer>
