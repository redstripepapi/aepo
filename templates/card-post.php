<article itemscope itemtype="http://schema.org/Article" <?php post_class(); ?>>
  <a class="post-card" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" itemprop="url">
    <figure class="post-card-thumb" style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></figure>
    <div class="post-card-content">
      <div>
        <header itemprop="name">
          <h4 class="post-card-title"><?php the_title(); ?></h4>
        </header>
        <main class="post-card-content-extended">
          <time class="post-card-date" itemprop="datePublished"><?php echo get_the_date( 'd M Y') ?></time>
          <p class="post-card-excerpt"><?php echo get_the_excerpt(); ?></p>
          <span class="post-card-read-all">Read all</span>
        </main>
      </div>
    </div>
  </a>
</article>
