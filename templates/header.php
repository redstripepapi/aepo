<header class="banner">
  <div class="container">
    <div class="row">
      <a class="brand" href="<?= esc_url(home_url('/')); ?>"><i class="aepo-icon aepo-icon--brand"><?php bloginfo('name'); ?></i></a>
      <nav class="nav-primary">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
        endif;
        ?>
      </nav>
      <a class="nav-icon" href="#">
        <span></span>
        <span></span>
        <span></span>
      </a>
    </div>
  </div>
</header>
