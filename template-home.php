<?php
/**
 * Template Name: Home Template
 */

  while (have_posts()) : the_post();

    if( have_rows( 'page_builder' ) ):

      while ( have_rows( 'page_builder' ) ) : the_row();

        get_template_part( 'page-builder/content-' . str_replace( '_', '-', get_row_layout() ) );

      endwhile;

    else :

      echo '<p>No content</p>';

    endif;

  endwhile;

?>
