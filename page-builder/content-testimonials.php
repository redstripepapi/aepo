<section class="testimonials content-block content-block-box content-block-carousel">
  <div class="container">
    <h4 class="content-block-header"><?php the_sub_field( 'content_block_header' ); ?></h4>
    <div class="testimonials-carousel content-block-carousel owl-carousel">
      <?php while ( have_rows('testimonial') ) : the_row(); ?>
        <?php $x = 0; while( $x < 3 ) : $x++; //Looped for demo purposes - dummy copy only ?>
          <li class="testimonial">
            <?php $image = get_sub_field( 'testimonial_author_profile_image' ); ?>
            <?php if( !empty($image) ) : ?>
              <figure>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
              </figure>
            <?php endif; ?>
            <blockquote cite="">
              <p><?php the_sub_field( 'testimonial_content' ); ?></p>

              <footer class="author">
                <strong class="author-name"><?php the_sub_field( 'testimonial_author_name' ); ?></strong>
                <span class="author-role"><?php echo get_sub_field( 'testimonial_author_job_role' ) . ' / ' . get_sub_field( 'testimonial_author_company' );  ?></span>
              </footer>
            </blockquote>
          </li>
        <?php endwhile; ?>
      <?php endwhile; ?>
    </div>
  </div>
</section>
