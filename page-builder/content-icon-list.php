<section class="content-block content-block-box">
  <div class="container">
    <h4 class="content-block-header"><?php the_sub_field( 'content_block_header' ); ?></h4>
    <div class="icon-list">
      <?php while ( have_rows('list_item') ) : the_row(); ?>
        <li>
          <?php echo ( !empty( get_sub_field( 'list_item_icon' ) ) ? '<figure><i class="aepo-icon aepo-icon--' . get_sub_field( 'list_item_icon' ) . '"></i></figure>' : '' ) ; ?>
          <p><?php the_sub_field( 'list_item_copy' ); ?></p>
        </li>
      <?php endwhile; ?>
    </div>
  </div>
</section>
