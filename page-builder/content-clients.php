<section class="content-block content-block-box">
  <div class="container">
    <h4 class="content-block-header"><?php the_sub_field( 'content_block_header' ); ?></h4>
    <div class="clients clients-carousel">
      <?php while ( have_rows('client') ) : the_row(); ?>
        <li>
          <figure class="client-logo">
            <?php $logo = get_sub_field( 'client_logo' ); ?>
            <img src="<?php echo $logo['url']; ?>" style="width: <?php echo $logo['width']/2; ?>px" />
          </figure>
        </li>
      <?php endwhile; ?>
    </div>
  </div>
</section>
