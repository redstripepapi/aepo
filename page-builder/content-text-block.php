<section class="content-block content-block-box content-block-box-tight">
  <div class="container">
    <h4 class="content-block-header"><?php the_sub_field( 'content_block_header' ); ?></h4>
    <p class="content-block-paragraph"><?php echo get_sub_field( 'text_block_content' ); ?></p>
    <?php if( have_rows( 'buttons' ) ) : ?>
      <div class="content-block-buttons">
        <?php while ( have_rows('buttons') ) : the_row(); ?>
          <a class="button" href="<?php the_sub_field( 'button_link' ); ?>"><?php the_sub_field( 'button_text' ); ?></a>
        <?php endwhile; ?>
      </div>
    <?php endif; ?>
  </div>
</section>
