<?php if( have_rows('hero_slide') ): ?>
  <section class="hero-slider owl-carousel content-block">
    <?php while ( have_rows('hero_slide') ) : the_row(); ?>
      <?php $x=0; while( $x<2 ): $x++; //Looped for demo purposes - dummy copy only ?>
        <li class="hero-slide">
          <div class="hero-slide-copy-wrapper">
            <h2 class="hero-slide-copy hero-slide-copy-header"><?php the_sub_field('hero_slide_header'); ?></h2>
            <h4 class="hero-slide-copy hero-slide-copy-subheader"><?php the_sub_field('hero_slide_subheader'); ?></h4>
          </div>
          <div class="hero-slide-image-element">
            <?php $image = get_sub_field( 'hero_slide_image' ); ?>
            <figure style="background-image: url(<?php echo $image['url']; ?>)"></figure>
          </div>
        </li>
      <?php endwhile; ?>
    <?php endwhile; ?>
  </section>
<?php endif; ?>
