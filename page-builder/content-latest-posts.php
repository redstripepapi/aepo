
  <?php
    $args = array(
      'posts_per_page' => 3
    );
  ?>
  <?php $lp_loop = new WP_Query( $args ); ?>
  <?php if( $lp_loop->have_posts() ) : ?>
    <section class="latest-posts content-block content-block-box">
      <div class="container">
        <h4 class="content-block-header"><?php the_sub_field( 'content_block_header' ); ?></h4>
        <div class="row">
          <?php while( $lp_loop->have_posts() ) : $lp_loop->the_post(); ?>
            <?php get_template_part( 'templates/card', 'post' ); ?>
          <?php endwhile; wp_reset_postdata(); ?>
        </div>
      </div>
    </section>
  <?php endif; ?>
